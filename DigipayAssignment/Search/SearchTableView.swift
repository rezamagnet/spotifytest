//
//  SearchTableView.swift
//  DigipayAssignment
//
//  Created by Reza Khonsari on 10/22/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import UIKit

struct SearchTableView {
    
    var tableView : UITableView
    
    
    init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    func empty() {
        tableView.backgroundView = Bundle.main.loadNibNamed(Strings.emptySearchView, owner: self, options: nil)?.first as? UIView
        tableView.separatorStyle = .none
    }
    
    
    let searchBarController : UISearchController = {
        var customSearchController = UISearchController(searchResultsController: nil)
        customSearchController.obscuresBackgroundDuringPresentation = false
        customSearchController.searchBar.placeholder = Strings.searchBarPlaceHolder
        return customSearchController
    }()

    enum Strings {
        static let emptySearchView = "EmptySearchView"
        static let navigationSearchTitle = "Search"
        static let searchBarPlaceHolder = "tracks..."
        static let searchCellIdentifier = "TrackCell"
        static let trackDetailSegue = "TrackDetailSegue"
    }
}
