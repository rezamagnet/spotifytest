//
//  Search.swift
//  DigipayAssignment
//
//  Created by Reza Khonsari on 10/22/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import Foundation
import RKNetwork
import RxSwift
import RxCocoa


let jsonDecoder = JSONDecoder()



struct Search : Decodable {
    
    let tracks : SearchTracks
    static var searchItems : BehaviorRelay<[SearchItem]> = BehaviorRelay(value: [])
    
    static func searchResult(with text: String) {
        
        let paramters = [
            "q":text,
            "type":"track"
        ]
        
        Network.request(with: .search(parameters: paramters))  { data in
            do {
                let result = try jsonDecoder.decode(Search.self, from: data)
                searchItems.accept(result.tracks.items)
                print(result.tracks.items)
            } catch {
                print(error)
            }
            
        }
    }
    
    
    enum CodingKeys: String,CodingKey {
        case tracks
    }
    
}

struct SearchTracks : Decodable {
    let items : [SearchItem]
}

struct SearchItem : Decodable {
    let name : String
    let album : SearchItemAlbum
    
}

struct SearchItemAlbum : Decodable {
    let images : [SearchItemImage]
}

struct SearchItemImage : Decodable {
    let url : String
}
