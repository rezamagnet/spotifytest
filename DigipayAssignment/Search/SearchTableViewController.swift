//
//  SearchTableViewController.swift
//  DigipayAssignment
//
//  Created by Reza Khonsari on 10/22/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class SearchTableViewController: UITableViewController {
    
    
    private lazy var searchTableView = SearchTableView(tableView: tableView)
    private let disposeBag = DisposeBag()
    private var search = Observable.just(Search.searchItems).merge()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTitle()
        searchTableView.empty()
        addSearchBar()
        searchBarObserver()
        getData()
        goToNextPage()
    }
    
    
    private func goToNextPage() {
        tableView.rx.modelSelected(SearchItem.self).subscribe(onNext: { item in
            self.performSegue(withIdentifier: SearchTableView.Strings.trackDetailSegue, sender: item)
        }).disposed(by: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let trackViewController = segue.destination as? TrackViewController {
            trackViewController.set(item: sender as! SearchItem)
        }
    }
    
    
    private func searchBarObserver() {
        searchTableView.searchBarController.searchBar.rx.text.orEmpty
            .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { query in
                Search.searchResult(with: query)
            }).disposed(by: disposeBag)
    }
    
    
    private func getData() {
        search.bind(to: tableView.rx.items(cellIdentifier: SearchTableView.Strings.searchCellIdentifier)) { row, track , cell in
            cell.textLabel?.text = track.name
        }.disposed(by: disposeBag)
    }
    
    
    private func addTitle() {
        title = SearchTableView.Strings.navigationSearchTitle
    }
    
    
    private func addSearchBar() {
        definesPresentationContext = true
        navigationItem.searchController = searchTableView.searchBarController
    }

}

