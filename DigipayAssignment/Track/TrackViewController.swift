//
//  TrackViewController.swift
//  DigipayAssignment
//
//  Created by Reza Khonsari on 10/22/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import UIKit

class TrackViewController: UIViewController {

    @IBOutlet weak var trackImageView: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    
    private var searchItem : SearchItem?
    
    func set(item: SearchItem) {
        searchItem = item
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewItem()
    }
    
    
    private func setViewItem() {
        if let searchItem = searchItem,
            let image = searchItem.album.images.first {
            trackImageView.RK(from: image.url)
            trackName.text = searchItem.name
        }
    }
    
}
