//
//  ViewController.swift
//  DigipayAssignment
//
//  Created by Reza Khonsari on 10/21/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    lazy private var spotifyLogginButton = Spotify.setLoginButton(with: self)
}



// MARK: - viewDidLoad
extension LoginViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        addSpotifyLoginButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        spotifyLoginHandler()
    }
}

// MARK: - Spotify Login Handler
extension LoginViewController {
    private func spotifyLoginHandler() {
        Spotify.shared.getAccessToken { [weak self] (token, error) in
            guard let `self` = self else { return }
            if error == nil {
                Spotify.token = token
                self.spotifyLoginSuccessful()
            } else {
                self.spotifyLogginButton.isEnabled = true
                NotificationCenter.default.addObserver(self,
                                                       selector: #selector(self.spotifyLoginSuccessful),
                name: .SpotifyLoginSuccessful,
                object: nil)
            }
        }
    }
    
    @objc private func spotifyLoginSuccessful() {
        NotificationCenter.default.removeObserver(self)
        performSegue(withIdentifier: "SearchTableViewControllerSegue", sender: self)
    }
}

// MARK: - Spotify Login Button Customization
extension LoginViewController {
    private func addSpotifyLoginButton() {
        view.addSubview(spotifyLogginButton)
        spotifyLogginButton.translatesAutoresizingMaskIntoConstraints = false
        spotifyLogginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spotifyLogginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        spotifyLogginButton.isEnabled = false
    }
}


