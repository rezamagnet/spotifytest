//
//  Spotify.swift
//  DigipayAssignment
//
//  Created by Reza Khonsari on 10/21/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import Foundation
import SpotifyLogin


// MARK: - Spotify Config
enum Spotify {
    
    static let shared = SpotifyLogin.shared
    static let clientId = "ba05b9cd59634cefa8493ac961d76ed6"
    static let secretId = "80b7235a88264654a105a989f6775a59"
    static let digipayRedirectURL = URL(string: "dpg://mydigipay/")!
    
    
    static var token : String? {
        get {
            return UserDefaults.standard.string(forKey: "Token")
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "Token")
        }
    }
    
    
    static func configuration() {
        shared.configure(clientID: clientId, clientSecret: secretId, redirectURL: digipayRedirectURL)
    }
    
    
    static func openURLHanlder(url: URL) -> Bool {
        return shared.applicationOpenURL(url) { (error) in }
    }
    
    
}



// MARK: - Spotify View
extension Spotify {
    static func setLoginButton(with viewcontroller : UIViewController) -> SpotifyLoginButton {
        return SpotifyLoginButton(viewController: viewcontroller, scopes: [.streaming,.userLibraryRead])
    }
    
    
    static func presentLogin(with viewcontroller : UIViewController) {
        SpotifyLoginPresenter.login(from: viewcontroller, scopes: [.streaming,.userLibraryRead])
    }
}
