//
//  Extension+ImageDownload.swift
//  DigipayAssignment
//
//  Created by Reza Khonsari on 10/22/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import UIKit


extension UIImageView {
    func RK(from url: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        if let url = URL(string: url) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else { return }
                DispatchQueue.main.async() {
                    self.image = image
                }
            }.resume()
        }
    }
}
