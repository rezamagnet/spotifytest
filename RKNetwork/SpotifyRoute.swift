//
//  Router.swift
//  RKNetwork
//
//  Created by Reza Khonsari on 10/22/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import Foundation

public enum SpotifyRoute : URLRequestConvertible {

    
    
    static let baseURL = "https://api.spotify.com/v1/"
    
    var token : String? {
        return UserDefaults.standard.string(forKey: "Token")
    }
    
    case search(parameters: Parameters)
    
    var method : HTTPMethod {
        switch self {
        case .search: return .get
        }
    }
    
    
    var path : String {
        switch self {
        case .search: return "search"
        }
    }
    
    
    
    public func asURLRequest() throws -> URLRequest {
        let url = try SpotifyRoute.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 8
        if let token = token {
            urlRequest.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            print(token)
        }
        
        
        
        switch self {
        case .search(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        return urlRequest
    }
    
    
}



