//
//  RKNetwork.swift
//  RKNetwork
//
//  Created by Reza Khonsari on 10/22/19.
//  Copyright © 2019 Reza Khonsari. All rights reserved.
//

import Foundation

public class Network {
    
    static let session = URLSession.shared
    
    public static func request(with route: SpotifyRoute,complitionHandler: @escaping (_ data: Data) -> Void) {
        let requestTask = session.dataTask(with: try! route.asURLRequest()) { data, response, error in
            if let data = data {
                complitionHandler(data)
            }
        }
        requestTask.resume()
    }
    
}
